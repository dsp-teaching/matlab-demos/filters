fp=0.2;
fa=0.3;
f_nom =(fp+fa)/2;

M=1000;
t = -M:M;

N=[100, 200, 300];

h_nom = 0.5*sinc(t*2*f_nom);

plot(w/(2*pi), abs(H))


figure(1)
clf

labels = {};

for k=1:length(N)
  h=h_nom(find(abs(t) < N(k)/2));

  [H,w]=freqz(h, 1, 4096);
  plot(w/(2*pi), 20*log10(abs(H)))
  hold on

  labels{end+1} = sprintf('N=%d', N(k));
end

legend(labels)
xlim([0.2 0.3])
ylim([-30 10])
grid on
% pause

figure(2)
clf

figure(3)
clf

labels = {};

finestre = {@() 1, @bartlett, @hamming};
L=101;

h_0 = h_nom(find(abs(t) < L/2));
for k=1:length(finestre)
  win = finestre{k};
  h=h_0(:) .* win(L);

  [H,w]=freqz(h, 1, 4096);

  figure(2)
  plot(w/(2*pi), 20*log10(abs(H)))
  hold on

  figure(3)
  plot(w/(2*pi), (abs(H)))
  hold on

  labels{end+1} = disp(win);
end

figure(2)

legend(labels)
xlim([0.2 0.3])
ylim([-70 10])
grid on

figure(3)

legend(labels)
xlim([0.2 0.3])
grid on


figure(4)
clf

h_trapezio = 0.5*sinc(t*2*f_nom) .* sinc(t*(fa-fp));

labels = {};

for k=1:length(N)
  h=h_trapezio(find(abs(t) < N(k)/4));

  [H,w]=freqz(h, 1, 4096);
  plot(w/(2*pi), (abs(H)))
  hold on

  labels{end+1} = sprintf('N=%d', N(k)/2);
end

legend(labels)
grid on
% pause
