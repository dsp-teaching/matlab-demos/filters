end_pass_band = 0.3;
start_stop_band = 0.4;
Rp = 3;
Ra = 40;

[N_butter, wc] = buttord(end_pass_band, start_stop_band, Rp, Ra);
[butter_num, butter_den] = butter(N_butter, wc);

[H_butter, W] = freqz(butter_num, butter_den);

[N_cheby1, wc] = cheb1ord(end_pass_band, start_stop_band, Rp, Ra);
[cheby1_num, cheby1_den] = cheby1(N_cheby1, Rp, wc);

[H_cheby1, W] = freqz(cheby1_num, cheby1_den);

[N_cheby2, wc] = cheb2ord(end_pass_band, start_stop_band, Rp, Ra);
[cheby2_num, cheby2_den] = cheby2(N_cheby2, Ra, wc);

[H_cheby2, W] = freqz(cheby2_num, cheby2_den);

[N_ellip, wc] = ellipord(end_pass_band, start_stop_band, Rp, Ra);
[ellip_num, ellip_den] = ellip(N_ellip, Rp, Ra, wc);

[H_ellip, W] = freqz(ellip_num, ellip_den);

db = @(x) 20*log10(abs(x));


w = W / pi;

figure(1)
clf
plot(W, db(H_butter), ...
     W, db(H_cheby1), ...
     W, db(H_cheby2), ...
     W, db(H_ellip));

ylim([-50, 3])
grid on
xlabel('Normalized frequency (matlab style)')

legend(sprintf('Butterworth N=%d', N_butter), ...
       sprintf('Chebyshev 1 N=%d', N_cheby1), ...
       sprintf('Chebyshev 2 N=%d', N_cheby2), ...
       sprintf('Elliptic N=%d', N_ellip))

figure(2)
clf
subplot(2,2,1)
zplane(butter_num, butter_den);
title('Butterwoth')

subplot(2,2,2)
zplane(cheby1_num, cheby1_den);
title('Chebyshev 1')

subplot(2,2,3)
zplane(cheby2_num, cheby2_den);
title('Chebyshev 2')

subplot(2,2,4)
zplane(ellip_num, ellip_den);
title('Elliptic')


figure(3)
clf
u = [1 zeros(1,99)];
t = 0:length(u)-1;

impulse_response = @(num, den) filter(num, den, u)


plot(t, impulse_response(butter_num, butter_den), ...
     t, impulse_response(cheby1_num, cheby1_den), ...
     t, impulse_response(cheby2_num, cheby2_den), ...
     t, impulse_response(ellip_num, ellip_den))


legend(sprintf('Butterworth N=%d', N_butter), ...
       sprintf('Chebyshev 1 N=%d', N_cheby1), ...
       sprintf('Chebyshev 2 N=%d', N_cheby2), ...
       sprintf('Elliptic N=%d', N_ellip))

